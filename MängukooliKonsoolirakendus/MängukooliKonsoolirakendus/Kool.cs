﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MängukooliKonsoolirakendus
{
    class Inimene
    {
        // staatilised asjad
        static Dictionary<string, Inimene> _Inimesed
            = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed
            => _Inimesed.Values;
        public static Inimene ByCode(string isikukood)
            => OnOlemas(isikukood) ? _Inimesed[isikukood] : null;
        public static bool OnOlemas(string isikukood)
            => _Inimesed.ContainsKey(isikukood);

        // inimese asjad
        public string Isikukood { get; private set; }
        public string Nimi { get; set; }
        //TODO: siia võiks suurtähe kontrolli lisada

        // readonly property koos muutmismeetodiga (hiljem saab lisada kontrolli)
        public string Klass { get; private set; } = "";
        public void ViiKlassi(string uusKlass) => Klass = uusKlass;
        public bool KasÕpilane => Klass != "";

        // readonly property koos muutmismeetodiga (hiljem saab lisada kontrolli)
        public string Aine { get; private set; } = "";
        public void MääraAine(string uusAine) => Aine = uusAine;
        public bool KasÕpetaja => Aine != "";

        List<string> _Lapsed = new List<string>(); // laste isikukoodid
        List<string> _Vanemad = new List<string>(); // vanemate isikukoodid

        private Inimene(string isikukood)
        { 
                //if (!_Inimesed.ContainsKey(isikukood))
                _Inimesed.Add(isikukood, this);
        }

        public static Inimene New(string isikukood, string nimi, string klass="", string aine="")
        {
            if (!OnOlemas(isikukood)) return new Inimene(isikukood) { Nimi = nimi, Klass = klass, Aine = aine };
            Inimene kes = ByCode(isikukood);
            kes.Aine = aine == "" ? kes.Aine : aine;
            kes.Klass = klass == "" ? kes.Klass : klass;
            return kes;
            
        }

        public void LisaLaps(string isikukood)
        {
            if (OnOlemas(isikukood))
            {
                this._Lapsed.Add(isikukood);
                ByCode(isikukood)._Vanemad.Add(this.Isikukood);
            }
        }

        public IEnumerable<Inimene> Lapsed => _Lapsed.Select(x => ByCode(x));
        public IEnumerable<Inimene> Vanemad => _Vanemad.Select(x => ByCode(x));

        public override string ToString()
            => Klass == "" ? "" : $"{Klass} õpilane "
            + Aine == "" ? "" : $"{Aine} õpetaja " + Nimi;

    }
}
